from collections import defaultdict
from itertools import chain, combinations
from time import time
import csv
import json
import operator

import config
 
class Rule(object):
    def __init__(self, elements, remain, support, confidence, lift, jaccard, conviction):
        self.elements = elements
        self.remain = remain
        self.support = support
        self.confidence = confidence
        self.lift = lift
        self.jaccard = jaccard
        self.conviction = conviction

class Apriori(object):
    def __init__(self):
        self.data = None
        self.itemsets = set()
        self.large_set = dict()
        self.freq_itemsets = list()
        self.freq_set = defaultdict(int)
        self.transaction_list = list()
        self.rules = list()

    @property
    def number_of_transactions(self):
        return len(self.transaction_list)

    @property
    def number_of_itemsets(self):
        return len(self.itemsets)

    @property
    def number_of_rules(self):
        return len(self.rules)

    @property
    def number_of_frequent_itemsets(self):
        return len(self.freq_itemsets)

    def readData(self, field='ingredients'):
        """ 
            Read the data inside the .json file provided by the user.
            The field provided it will define which field will be mined.
        """
        with open(config.DBJSON) as data_file:
            data = json.load(data_file)

        unique_items = set()

        for item in data:
            item_fields = item.get(field)
            for ingr in item_fields:
                unique_items.add(ingr)

        config.UNIQUE_ITEMS = list(unique_items)

        unique_items_index = set()
        for item in data:
            item_fields = item.get(field)
            for key,ingr in enumerate(item_fields):
                ingr_index = config.UNIQUE_ITEMS.index(ingr)
                item_fields[key] = ingr_index
                unique_items_index.add(ingr_index)

        for item in unique_items_index:
            self.itemsets.add(frozenset([item]))

        for item in data:
            transaction = frozenset(item.get(field))
            self.transaction_list.append(transaction)

    def returnItemsWithMinSupport(self, itemset):
        """ 
            Calculate support for the items in the itemset and returns
            a subset with the items that satisfies the minimum support
            defined by the user.
        """
        _itemSet = set()
        local_set = defaultdict(int)

        for item in itemset:
            for transaction in self.transaction_list:
                if item.issubset(transaction):
                    self.freq_set[item] += 1
                    local_set[item] += 1

        for item,support in local_set.items():
            if support >= config.MINSUP:
                _itemSet.add(item)

        return _itemSet

    def mineFrequentItemsets(self):
        """
            Mine frequent itemsets based on Apriori Algorithm.
        """
        current_LSet = self.returnItemsWithMinSupport(self.itemsets)

        k = 2
        while(current_LSet != set([])):
            self.large_set[k-1] = current_LSet
            current_LSet = self.joinSet(current_LSet, k)
            current_CSet = self.returnItemsWithMinSupport(current_LSet)

            current_LSet = current_CSet

            k = k+1

        for key,value in self.large_set.items():
            self.freq_itemsets.extend([(tuple(item), self.freq_set[item]) for item in value])

    def generateAssociationRules(self):
        """
            Generate the Association Rules based on the itemsets.
        """
        for key,value in self.large_set.items()[1:]:
            for item in value:
                _subsets = map(frozenset, [x for x in self.subsets(item)])
                for element in _subsets:
                    remain = item.difference(element)
                    if len(remain) > 0:
                        confidence = float(self.freq_set[item])/self.freq_set[element]
                        if confidence >= config.MINCONFIDENCE:
                            rsup = float(self.freq_set[item])/self.number_of_transactions
                            lift = confidence/(float(self.freq_set[remain])/self.number_of_transactions)
                            jaccard = float(self.freq_set[item])/(self.freq_set[remain] + self.freq_set[element] - self.freq_set[item])
                            try:
                                conviction = float(1-float(self.freq_set[remain]/self.number_of_transactions))/float(1-confidence)
                            except ZeroDivisionError:
                                # It means that the confidence is 100%, so there is no expected error for this rule
                                conviction = 0.0
                            self.rules.append((Rule(tuple(element), remain, rsup, confidence, lift, jaccard, conviction)))

    def printResults(self):
        """
            Print the Frequent Itemsets and the Association Rules generated.
        """
        print "Frequent Itemsets:"
        for item,support in sorted(self.freq_itemsets, key=lambda(item, support): support):
            formatted_item = [config.UNIQUE_ITEMS[tup_item] for tup_item in item]
            print 'item: {0}, {1}'.format(tuple(formatted_item), round(float(support)/self.number_of_transactions, 2))

        print "Rules:"
        for rule in sorted(self.rules, key=operator.attrgetter('confidence')):
            formatted_elems = [config.UNIQUE_ITEMS[tup_item] for tup_item in rule.elements]
            formatted_remain = [config.UNIQUE_ITEMS[tup_item] for tup_item in rule.remain]
            print 'Rule: {0} -> {1}\t{2}\t{3}\t{4}\t{5}\t{6}'.format(tuple(formatted_elems), tuple(formatted_remain), round(rule.support, 2), round(rule.confidence, 2), round(rule.lift, 2), round(rule.jaccard, 2), round(rule.conviction, 2))

    def generateStatistics(self):
        """
            Generate .csv files with statistics about the itemsets and rules mined
        """
        current_time = str(time()).split('.')[0]

        with open('itemsets_statistics_{0}.csv'.format(current_time), 'wb') as itemsets_statistics:
            itemsets_writer = csv.writer(itemsets_statistics, delimiter=';')
            for item,support in self.freq_itemsets:
                itemsets_writer.writerow([support, round(float(support)/self.number_of_transactions, 2)])

        with open ('rules_statistics_{0}.csv'.format(current_time), 'wb') as rules_statistics:
            rules_writer = csv.writer(rules_statistics, delimiter=';')
            for rule in self.rules:
                rules_writer.writerow([round(rule.support, 2), round(rule.confidence, 2), round(rule.lift, 2), round(rule.jaccard, 2), round(rule.conviction, 2)])
        
    def subsets(self, subset):
        """
            Returns non empty subsets of subset.
        """
        return chain(*[combinations(subset, i + 1) for i, a in enumerate(subset)])

    def joinSet(self, itemset, length):
        """
            Joins an itemset with itself and returns the nth-element itemsets.
        """
        return set([i.union(j) for i in itemset for j in itemset if len(i.union(j)) == length])
