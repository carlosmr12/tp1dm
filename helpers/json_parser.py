import json

with open('train.json') as data_file:
    data = json.load(data_file)

unique_ingredients = set()
unique_cuisine = set()

for item in data:
    unique_cuisine.update([item.get('cuisine')])
    item_ingredients = item.get('ingredients')

    for ingr in item_ingredients:
        unique_ingredients.update([ingr])

unique_ingredients_list = list(unique_ingredients)
unique_cuisine_list = list(unique_cuisine)

for item in data:
    item['cuisine'] = unique_cuisine_list.index(item.get('cuisine'))
    item_ingredients = item.get('ingredients')
    for key,ingr in enumerate(item_ingredients):
        item_ingredients[key] = unique_ingredients_list.index(ingr)
