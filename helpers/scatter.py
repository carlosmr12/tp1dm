# -*- coding: utf-8 -*-
from matplotlib import pyplot
import csv

rsup_list = []
confidence_list = []
lift_list = []
jaccard_list = []
conviction_list = []

with open('rules_statistics_1445859903.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=";")

    reader.next()

    for row in reader:
        rsup_list.append(float(row[0]))
        confidence_list.append(float(row[1]))
        lift_list.append(float(row[2]))
        jaccard_list.append(float(row[3]))
        conviction_list.append(float(row[4]))

#pyplot.scatter(rsup_list, confidence_list, s=50)
#pyplot.title(u"rsup x confidence")
#pyplot.xlabel(u"rsup")
#pyplot.ylabel(u"confidence")
#pyplot.savefig('rsupxconfidence.png')
#
#pyplot.scatter(lift_list, conviction_list, s=50)
#pyplot.title(u"lift x conviction")
#pyplot.xlabel(u"lift")
#pyplot.ylabel(u"conviction")
#pyplot.savefig('liftxconviction.png')
#
#pyplot.scatter(rsup_list, jaccard_list, s=50)
#pyplot.title(u"rsup x jaccard")
#pyplot.xlabel(u"rsup")
#pyplot.ylabel(u"jaccard")
#pyplot.savefig('rsupxjaccard.png')
#
#pyplot.scatter(jaccard_list, confidence_list, s=50)
#pyplot.title(u"confidence x jaccard")
#pyplot.xlabel(u"confidence")
#pyplot.ylabel(u"jaccard")
#pyplot.savefig('confidencexjaccard.png')

pyplot.scatter(lift_list, jaccard_list, s=50)
pyplot.title(u"lift x jaccard")
pyplot.xlabel(u"lift")
pyplot.ylabel(u"jaccard")
pyplot.savefig('liftxjaccard.png')
