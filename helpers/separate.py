# -*- coding: utf-8 -*-
from matplotlib import pyplot
import csv

rsup_list = []
rsup_unique = set()

with open('itemsets_statistics_1445859903.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=";")

    for row in reader:
        rsup_list.append(float(row[1]))
        rsup_unique.add(float(row[1]))

with open('separated.csv', 'wb') as fwriter:
    writer = csv.writer(fwriter, delimiter=';')
    writer.writerow(['rsup', 'count'])
    for item in sorted(rsup_unique):
        writer.writerow([item, rsup_list.count(item)])
