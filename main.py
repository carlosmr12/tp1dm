# -*- coding: utf-8 -*-

"""
TP1 – Mineração de Padrões Frequentes em uma Base de Dados de Receitas Típicas de Diferentes Países
===================================================================================================

Carlos Henrique Miranda Rodrigues
chmrodrigues@ufmg.br

DCC917 – Mineração de Dados 
Departamento de Ciência da Computação
Universidade Federal de Minas Gerais - UFMG
"""

from argparse import ArgumentParser

from apriori import Apriori
import config

def main():
    apriori = Apriori()

    apriori.readData()

    apriori.mineFrequentItemsets()

    apriori.generateAssociationRules()

    apriori.printResults()
    
    apriori.generateStatistics()

if __name__ == "__main__":
    parser = ArgumentParser(description=u'TP1 - Mineração de padrões frequentes - DCC917')

    parser.add_argument('minsup', metavar='minsup', default=15, nargs='?', type=int, help=u'Minimum support')
    parser.add_argument('minconfidence', metavar='minconfidence', default=0.8, nargs='?', type=float, help=u'Minimum confidence for rules')
    parser.add_argument('dbjson', metavar='dbjson', default=u'fixtures/default.json', nargs='?', type=str, help=u'.json database')

    args = parser.parse_args()

    config.MINSUP = args.minsup
    config.MINCONFIDENCE = args.minconfidence
    config.DBJSON = args.dbjson

    main()
